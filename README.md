![Dfinity Logo](/images/dfinity-logo-hero-sm.png)

### Dfinity Dev Community Resource List ###

This list is frequently updated. [Click here if you want to add a resource.](https://docs.google.com/document/d/1rxYOVau1oNIO09nb0OgJVKW8_Su1_njiAZUg-LeQ5n4/edit)



#### General ####

* https://github.com/dfinity



#### Informational Videos ####

* [Mission Control Youtube Channel](https://www.youtube.com/channel/UCN9GW_VfwtlKCR-vE2tsBww)
* [Norton Wang describes the actor model, web canisters, and discusses and runs some quick tests on the DFINITY SDK](https://www.youtube.com/watch?v=I4Qb3Dpb86o)
* [Mahnush Movahedi discusses the Distributed Key Generation protocol](https://www.youtube.com/watch?v=ZZ6w6DKr6qs])
* [Scalable consensus (Timo Hanke at BPASE 2018) ](https://www.youtube.com/watch?v=9HRurPVF3Pg)
* [Methods for Deterministic Parallelizing Message Processing (Martin Becze at Devcon3, November 2017)](https://www.youtube.com/watch?v=xS5sW6gJIEM)
* [From eWASM to Primea (Martin Becze at EthCC, March 2018)](https://www.youtube.com/watch?v=ZsXyDIKcurw)
* [Primea and WASM in DFINITY (Martin Becze at DFINITY/TrueBit event, April 2018)](https://www.youtube.com/watch?v=8xf0eBK_Ry0)
* [Martin Becze: Primea – The Next-Generation Blockchain Operating System](https://www.youtube.com/watch?v=ovGM2FTq0Kk)
* [Zero Knowledge Episode 14: Diving deep into DFINITY](http://www.zeroknowledge.fm/14)  

#### Mission Control Developer AMAs: ####

1. https://www.youtube.com/watch?v=nL2ga5s49xA
2. https://www.youtube.com/watch?v=464UHhXi1TY
3. https://www.youtube.com/watch?v=CGLlvXtkHwI
4. https://www.youtube.com/watch?v=SepRgW__-5w
5. https://www.youtube.com/watch?v=8N9dLkTqznE
6. https://youtu.be/tJk2uLSHtm4
7. https://youtu.be/Td8XX21mPX4
8. https://youtu.be/5Pq0Rb1kAEk

#### Podcasts ####
* [The Third Web by Arthur Falls - Dom interviewed, consensus paper reviewed. Link to iTunes](https://itunes.apple.com/us/podcast/the-third-web-4-dfinity-consensus/id899090462?i=1000400556750&mt=2)

* [Diving deep into DFINITY - Interview with Robert Lauko.](http://www.zeroknowledge.fm/14)

#### Primea ####
_Primea repo and some doc reading for those interested:_

* https://github.com/primea
* https://github.com/primea/js-primea-hypervisor/tree/master/docs
* https://github.com/primea/design?files=1
* https://github.com/primea/design/issues

* Original eWasm docs: https://github.com/ewasm/design




#### Actor model ####

* http://dist-prog-book.com/chapter/3/message-passing.html
* [Hewitt, Meijer and Szyperski: The Actor Model (everything you wanted to know...)](https://www.youtube.com/watch?v=7erJ1DV_Tlo)



#### WebAssembly ####

* [Awesome lists](https://github.com/mbasso/awesome-wasm/blob/master/README.md)
* [Intro to WebAssembly](https://rsms.me/wasm-intro)
* [Wasm studio](https://webassembly.studio/)
* [Google Developers Codelabs - An Introduction to Web Assembly tutorial](https://codelabs.developers.google.com/codelabs/web-assembly-intro/index.html)



#### Haskell ####

* http://www.happylearnhaskelltutorial.com/contents.html
* [An excellent Haskell resource list on stackoverflow](https://stackoverflow.com/questions/1012573/getting-started-with-haskell)
* [School Of Haskell](https://www.schoolofhaskell.com/)
* [Haskell Sorcery by Ben Lynn](https://crypto.stanford.edu/~blynn/haskell/)

#### Community Member Projects & Initiatives ####

* DFINITY Explorer is an open-source block explorer for the DFINITY blockchain - https://github.com/dfinityexplorer/dfinityexplorer - Contact the DFINITY Explorer team at dfinityexplorer[at]gmail.com for more info.
* Decentralized Exchange using the DFINITY Consensus Protocol - https://github.com/helinwang/dex
